package nl.roundtree;

public class Application {

    public static void main(final String... args) {
        System.out.println(Client.processInput(System.in));
    }
}
