package nl.roundtree;

import nl.roundtree.domain.Robot;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class RobotControllerTest {

    private RobotController robotController;
    private Robot robot;

    @Before
    public void setUp() {
        robot = mock(Robot.class);
        robotController = new RobotController(robot);
    }

    @Test
    public void testRotateLeft() {
        robotController.executeCommand('L');
        verify(robot).rotateLeft();
    }

    @Test
    public void testRotateRight() {
        robotController.executeCommand('R');
        verify(robot).rotateRight();
    }

    @Test
    public void testMoveForward() {
        robotController.executeCommand('F');
        verify(robot).moveForward();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInvalidCommand() {
        robotController.executeCommand('Q');
    }
}