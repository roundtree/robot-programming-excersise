package nl.roundtree.domain;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class RobotTest {

    @Test(expected = NullPointerException.class)
    public void testCreateRobotRoomNull() {
        new Robot(null, new Field(0, 0), Direction.E);
    }

    @Test(expected = NullPointerException.class)
    public void testCreateRobotStartFieldNull() {
        new Robot(new Room(1, 1), null, Direction.E);
    }

    @Test(expected = NullPointerException.class)
    public void testCreateRobotDirectionIsNull() {
        new Robot(new Room(1, 1), new Field(0, 0), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateRobotInvalidStartField() {
        new Robot(new Room(1, 1), new Field(2, 2), Direction.N);
    }

    @Test
    public void testRotateLeft() {
        final Robot robot = new Robot(new Room(1, 1), new Field(0, 0), Direction.E);
        robot.rotateLeft();
        assertThat(robot).hasFieldOrPropertyWithValue("direction", Direction.N);
    }

    @Test
    public void testRotateRight() {
        final Robot robot = new Robot(new Room(1, 1), new Field(0, 0), Direction.E);
        robot.rotateRight();
        assertThat(robot).hasFieldOrPropertyWithValue("direction", Direction.S);
    }

    @Test
    public void testMoveForward() {
        final Robot robot = new Robot(new Room(1, 1), new Field(0, 0), Direction.E);
        robot.moveForward();
        assertThat(robot).hasFieldOrPropertyWithValue("field", new Field(1, 0));
    }

    @Test
    public void testToString() {
        final Robot robot = new Robot(new Room(1, 1), new Field(0, 0), Direction.E);
        assertThat(robot.toString()).isEqualTo("Report: 0 0 E");
    }

    @Test
    public void testEqualsHashCode() {
        final Robot robot1 = new Robot(new Room(1, 1), new Field(0, 0), Direction.E);
        final Robot robot2 = new Robot(new Room(1, 1), new Field(0, 0), Direction.E);
        final Robot robot3 = new Robot(new Room(1, 1), new Field(1, 0), Direction.N);

        assertThat(robot1).isEqualTo(robot2);
        assertThat(robot2).isNotEqualTo(robot3);
        assertThat(robot1.hashCode()).isEqualTo(robot2.hashCode());
        assertThat(robot2.hashCode()).isNotEqualTo(robot3.hashCode());
    }
}